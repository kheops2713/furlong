const talgo230 = {
  label: 'Talgo',
  highSpeed: false,
  gaugeChange: true,
  weight: 320,
  cars: 14,
  multipleUnit: false,
  capacity: 200,
};

const talgo250 = {
  label: 'Talgo 250',
  highSpeed: true,
  gaugeChange: true,
  weight: 330,
  cars: 12,
  multipleUnit: false,
  capacity: 160,
};

export { talgo230, talgo250 };
